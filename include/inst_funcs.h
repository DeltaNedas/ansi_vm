#pragma once

#include "vm.h"

/* Utility functions for instructions and VM */

/* Will NOT wrap around memory end */
byte *get_byte(vm_t *vm, int addr);
/* Will wrap around memory end */
byte *next_byte(vm_t *vm, int offset);
int set_byte(vm_t *vm, int addr, byte set);
/* Return value in address pointed to after current byte */
byte *get_addr(vm_t *vm, int offset);
/* %FLAGS |= or */
byte set_flag(vm_t *vm, byte or);
/* %FLAGS & and */
byte has_flag(vm_t *vm, byte and);

/* Instructions */

int instf_halt(vm_t *vm);
int instf_print(vm_t *vm);
int instf_write(vm_t *vm);

int instf_set(vm_t *vm);
int instf_copy(vm_t *vm);

int instf_jmp(vm_t *vm);
int instf_jmp_eq(vm_t *vm);
int instf_jmp_lt(vm_t *vm);
int instf_jmp_gt(vm_t *vm);

int instf_lshift(vm_t *vm);
int instf_rshift(vm_t *vm);
int instf_not(vm_t *vm);
int instf_or(vm_t *vm);
int instf_and(vm_t *vm);
int instf_xor(vm_t *vm);

int instf_add(vm_t *vm);
int instf_sub(vm_t *vm);
int instf_mul(vm_t *vm);
int instf_div(vm_t *vm);
int instf_pow(vm_t *vm);
int instf_mod(vm_t *vm);

int instf_push(vm_t *vm);
int instf_pop(vm_t *vm);
