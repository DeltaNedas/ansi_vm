#pragma once

#include "registers.h"

#define DEFAULT_MEMORY 32

typedef unsigned char byte;

/* Virtual machine for my cpu */
typedef struct {
	/* number of bytes in memory */
	int memory_size;

	/* Continuous block of memory where
	   instructions and data are stored */
	byte *memory;
	/* Set to memory + %IP every step */
	byte *current_byte;

	/* File to load ROM from, NULL if using default instructions. */
	char *rom_path;

	byte registers[REGISTER_COUNT];
} vm_t;

/* This will:
	1. allocate vm struct
	2. parse options
	3. allocate vm memory
	4. return vm struct
If an error occurs, errno is set and
NULL (0) is returned.
The message is printed to stderr. */
vm_t *create_vm(int argc, char **argv);

/* load instructions to run */
int init_vm(vm_t *vm);

/* Stopped by HALT instruction (0x00) */
int run_vm(vm_t *vm);

void free_vm(vm_t *vm);
