#pragma once

#include "vm.h"

typedef int(*INST_FUNC)(vm_t*);

enum instruction_set {
/* MISC INSTRUCTIONS */
	/* quit run_vm() */
	HALT,
	/* print data at *A */
	PRINT,
	/* write a null terminated string *A to stdout */
	WRITE,

/* MEMORY OPERATIONS */
	/* set *A to B */
	SET,
	/* copy *A to *B */
	COPY,

/* JUMPING */
	/* jump to *A */
	JMP,
	/* " if *B is equal to C */
	JMP_EQ,
	/* "" less than C */
	JMP_LT,
	JMP_GT,

/* BITWISE OPS, USING %AX */
	/* shift A bits to the left */
	B_LS,
	/* " to the right */
	B_RS,
	/* invert */
	B_NOT,
	/* or each bit with A */
	B_OR,
	/* and " */
	B_AND,
	/* xor " */
	B_XOR,

/* INTEGER MATH, USING %AX */
	/* add A */
	ADD,
	/* subtract A */
	SUB,
	/* multiply by A */
	MUL,
	/* divide by A */
	DIV,
	/* raise to Ath power */
	POW,
	/* remainder of %AX / A */
	MOD,

/* STACK */
	/* push A to the stack */
	PUSH,
	/* pop value at head of stack */
	POP,

	/* not an instruction, just how many instructions there are */
	INST_COUNT
};

typedef struct {
	/* how many bytes it reads */
	int skip;
	INST_FUNC func;
} inst_t;

extern const inst_t instructions[];
