#pragma once

enum {
	VERBOSE = 0x01
};

enum {
	/* Arithmetic */
	AX,
	/* Counter, for loops */
	CX,
	/* Instruction pointer */
	IP,
	FLAGS,
	/* Stack Base, >= SP */
	SB,
	/* Stack Pointer */
	SP,

	REGISTER_COUNT
};
