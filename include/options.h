#pragma once

#ifdef USE_DECIMAL
#	define NUMF "%d"
#else
#	define NUMF "0x%x"
#endif
