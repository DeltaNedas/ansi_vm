#!/usr/bin/env lua

--[[
	Assemble instruction names into a rom.
	Run with input and output files.
	Defaults are stdin/out.
]]

local core = require("tools.core")

local input = io.stdin
local output = io.stdout

if arg[1] and arg[1] ~= "-" then
	local err
	input, err = io.open(arg[1], "rb")
	if not input then
		printe("* Failed to open input: %s\n", err)
		os.exit(1)
	end
end
if arg[2] and arg[2] ~= "-" then
	output, err = io.open(arg[2], "wb")
	if not output then
		printe("* Failed to open output: %s\n", err)
		os.exit(2)
	end
end

local function add(a, b)
	local l = #a
	for i, v in ipairs(b) do
		a[l + i] = b[i]
	end
end

local insts = core.instructions
local regs = core.registers

local aliases = {
	call = {
		"PUSH", "+0x03",
		"JMP" --[[ addr,
		return here ]]
	},
	ret = {
		"PEEK", "+0x03",
		"POP",
		"JMP", "0xFF"
	}
}

local parsers = {}
parsers.inst = function(word)
	local inst = insts[word:upper()]
	if inst then
		return {inst}
	end
end

parsers.offset = function(word)
	local offset = word:match("^([%+%-]0x%x%x?)$") or word:match("^([%+%-]%d+)$")
	if offset then
		return {word}
	end
end

parsers.num = function(word)
	local str = word:match("^0x%x%x?") or word:match("^[-+]?%d+$")
	if str then
		return {tonumber(str)}
	end
end

parsers.char = function(word)
	local char = word:match("^'(.)'$")
	if char then
		return {char:byte()}
	end
end

local parse

parsers.aliases = function(word)
	local alias = aliases[word:lower()]
	if alias then
		local ret = {}
		for i, v in ipairs(alias) do
			add(ret, parse(v))
		end
		return ret
	end
end

local source

parsers.label = function(word)
	local label = word:match("^%.(%w+)$")
	if label then
		-- it is compiled as the address isn't known yet
		return {word}
	end
end

parsers.label_offset = function(word)
	local label, offset = word:match("^%.(%w+)([%+%-].+)$")
	if offset and parsers.offset(offset) then
		return {word}
	end
end

parsers.reg = function(word)
	local reg = regs[word:lower():match("^%%(.+)$")]
	if reg then
		return {reg}
	end
end

parse = function(word)
	for name, parser in pairs(parsers) do
		local str = parser(word)
		if str then
			return str
		end
	end
end

local labels = {}

local function parse_label(name, source)
	local label = {
		name = name,
		source = source,
		line = 0, token = 0,
		bytes = {}
	}
	
	local ln = 0
	for line in source:gmatch("[^\n]+") do
		label.line = label.line + 1
		label.token = 0
		for word in line:gmatch("%S+") do
			if word:sub(1, 1) == ";" then break end

			label.token = label.token + 1
			local out = parse(word)
			if not out then
				printe("* Invalid token #%d on line #%d of label %s: '%s' (expected instruction, %%register, .label, number or 'character')\n", label.token, label.line, name, word)
				os.exit(3)
			end
			add(label.bytes, out)
		end
	end
	labels[name] = label
end

local compilers = {
	offset = function(word, i, label)
		if type(word) ~= "string" then return end
		if tonumber(word) then
			return i + tonumber(word) + label.addr - 1
		end
	end,
	label = function(word, i, label)
		if type(word) ~= "string" then return end
		local name = word:match("^%.(%w+)$")
		if name then
			local t = labels[name]
			if not t then
				printe("* Unknown label '%s' (token #%d of line #%d in label %s)\n",
					name, label.token, label.line, label.name)
				os.exit(5)
			end
			return t.addr
		end
	end
}

compilers.label_offset = function(word, i, label)
	if type(word) ~= "string" then return end
	local name, offset = word:match("^(%.%w+)([+-].+)$")
	if name and offset then
		return compilers.label(name, i, label) + tonumber(offset)
	end
end

local function compile_labels()
	for name, label in pairs(labels) do
		printe("Label '%s' at 0x%x\n", name, label.addr)
		for i, byte in pairs(label.bytes) do
			for cname, compiler in pairs(compilers) do
				local out = compiler(byte, i, label)
				if out then
					label.bytes[i] = out
					break
				end
			end
			label.bytes[i] = string.char(label.bytes[i])
		end
	end
end

source = input:read("a")
for label, code in source:gmatch("(%w+):%s*{(.-)}") do
	if labels[label] then
		printe("* Redeclared label '%s'\n", label)
		os.exit(4)
	end
	parse_label(label, code)
end
if not labels.main then
	printe("* No main label found.\n")
	os.execute(5)
end

local main = labels.main
main.addr = 0x00
local offset = #main.bytes
for name, label in pairs(labels) do
	if name ~= "main" then
		label.addr = offset
		offset = offset + #label.bytes
	end
end
compile_labels()

local function write_label(label)
	output:write(table.concat(label.bytes, ""))
end

write_label(main)
for name, label in pairs(labels) do
	if name ~= "main" then
		write_label(label)
	end
end
