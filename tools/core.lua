#!/usr/bin/env lua

-- Ansi VM source parsing code.

local core = {}

function printe(fmt, ...)
	return io.stderr:write(fmt:format(...))
end

local function read_sizes(path)
	local f, err, code = io.open(path)
	if not f then
		printe("* Failed to open %s for reading: %s\n", path, err)
		os.exit(code)
	end

	local structs = f:read("a")
		-- No comments
		:gsub("/%*.-%*/", "")
		-- Just get the widths, functions
		:gsub("instructions%[%] = {(.-)};", "%1")
		-- Strip whitespace
		:gsub("%s", "")

	core.sizes = {}
	core.functions = {}

	local i = 0
	for size, func in structs:gmatch("{(.-),(.-)}") do
		size = tonumber(size)
		core.sizes[i] = size == -1 and 0 or size
		core.functions[i] = func
		i = i + 1
	end
end

local function read_enums(path, name, setter)
	local f, err, code = io.open(path, "r")
	if not f then
		printe("* Failed to open %s for reading: %s\n", path, err)
		os.exit(code)
	end

	local data = f:read("a")
		-- strip comments
		:gsub("/%*.-%*/", "")
		-- required enums only
		:gsub("^.+enum.-{(.-)" .. name .. "_COUNT.+", "%1")
		-- comma separated array
		:gsub("%s", "")

	local ret = {}
	local enum = 0
	for word in data:gmatch("([^,]+),") do
		local name, value = word:match("(.+)=(.+)")
		if name then
			enum = tonumber(value)
		end

		setter(ret, name or word, enum)
		enum = enum + 1
	end
	return ret
end

read_sizes("src/instructions.c")
core.instructions = read_enums("include/instructions.h",
	"INST",
	function(ret, inst, enum)
		ret[inst] = enum
	end)
core.registers = read_enums("include/registers.h",
	"REGISTER",
	function(ret, reg, enum)
		ret[reg:lower()] = enum + 0x80
	end)

return core
