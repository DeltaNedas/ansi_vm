BUILDDIR ?= build
OBJECTDIR ?= objects
SOURCEDIR ?= src
INCLUDEDIR ?= include

UNAME := $(shell uname)
CC ?= gcc
CFLAGS ?= -O3 -Wall -ansi -pedantic -I$(INCLUDEDIR) -c -g
LDFLAGS ?= '-Wl,-rpath,$$ORIGIN' -g
LDFLAGS += -lm

BINARY ?= vm
SOURCES := $(shell find $(SOURCEDIR)/ -type f -name "*.c")
OBJECTS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.o, $(SOURCES))
DEPENDS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.d, $(SOURCES))

# Options, pass with NAME=VALUE
USE_DECIMAL ?= 0
ifeq ($(USE_DECIMAL), 1)
	CFLAGS += "-DUSE_DECIMAL"
endif

all: $(BUILDDIR)/$(BINARY)

$(OBJECTDIR)/%.o: $(SOURCEDIR)/%
	@printf "CC\t$@\n"
	@mkdir -p `dirname $@`
	@$(CC) $(CFLAGS) -MMD -MP $< -o $@

-include $(DEPENDS)

$(BUILDDIR)/$(BINARY): $(OBJECTS)
	@printf "CCLD\t$@\n"
	@mkdir -p $(BUILDDIR)
	@$(CC) $^ -o $@ $(LDFLAGS)

clean:
	rm -rf $(OBJECTDIR)

run: all
	@$(BUILDDIR)/$(BINARY)
