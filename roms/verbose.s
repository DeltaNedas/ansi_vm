; Test VM verbosity
main: {
	COPY	%flags	%ax
	B_AND		1
	JMP_EQ	.verbose	0x01
	JMP		.quiet
}

verbose: {
	WRITE	.vstr
	HALT
}
vstr: {
	'V' 'e' 'r' 'b' 'o' 's' 'e' 0x0a 0
}

quiet: {
	WRITE	.qstr
	HALT
}
qstr: {
	'Q' 'u' 'i' 'e' 't' 0x0a 0
}
