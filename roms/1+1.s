main: {
; Load the first number
	COPY	.str	%ax
; Add the second number
	ADD 	%ax		.str+4
; Save result
	COPY	%ax		.str+8
	WRITE	.str
	HALT
}

str: {
	'1' 0x20 '+' 0x20 '1' 0x20 '='
	0x20 0xFF 0x0a
	0x00
}
