main: {
	call	.func
	PRINT	%ax
	HALT
}

func: {
	call	.two
	ret
}

two: {
	COPY	%sb	%ax
	SUB	%ax	%sp
	ADD   .str+0x0e	%ax
	WRITE	.str
	ret
}

str: {
	'S' 't' 'a' 'c' 'k' 0x20
	's' 'i' 'z' 'e' 0x20
	'i' 's' 0x20 '0' 0x0a 0x00
}
