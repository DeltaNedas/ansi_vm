num: { 4 }

main: {
	JMP		.fac
}

fac: {
	COPY	.accum	%ax
	COPY	.num	+0x02
	MUL		0xFF
	COPY	%ax		.accum
	COPY	.num	%ax
	SUB		1
	JMP_EQ	.end	0
	COPY	%ax		.num
	JMP		.fac
}

end: {
	PRINT	.accum
	HALT
}

accum: { 1 }
