main: {
	PUSH	+7
	; Push a short 0x04F7
	PUSH	0x04
	PUSH	0xF7
	JMP		.func
	WRITE	.str
	HALT
}

str: {
	'N' 'u' 'm' 0x20 'i' 's' 0x20
	; .str+7 - 65565\n\0
	0 0 0 0 0 0 0
}
