; test out call stuff

; 0x00
main:
{
	SET		%ax		0x04
; ".func" points to ADD
	call	.func
	PRINT	%ax
	call	.func
	PRINT	%ax

	HALT
}

; add 3 to accum
func: {
	ADD		%ax		0x03
	JMP_EQ	0x03	%ax	0x0a
	ret
}
