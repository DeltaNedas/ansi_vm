#include "instructions.h"
#include "inst_funcs.h"

const inst_t instructions[] = {
	{-1, instf_halt},
	{2, instf_print},
	{2, instf_write},

	{3, instf_set},
	{3, instf_copy},

	{-1, instf_jmp},
	{3, instf_jmp_eq},
	{3, instf_jmp_lt},
	{3, instf_jmp_gt},

	{2, instf_lshift},
	{2, instf_rshift},
	{1, instf_not},
	{2, instf_or},
	{2, instf_and},
	{2, instf_xor},

	{2, instf_add},
	{2, instf_sub},
	{2, instf_mul},
	{2, instf_div},
	{2, instf_pow},
	{2, instf_mod},

	{2, instf_push},
	{1, instf_pop},
};
