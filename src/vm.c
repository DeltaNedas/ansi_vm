#include "vm.h"
#include "instructions.h"
#include "inst_funcs.h"
#include "options.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

/* VM's instruction cycle code */

int run_vm(vm_t *vm) {
	extern int errno;
	int code, status;
	inst_t inst;

	while (1) {
		errno = 0;
		vm->current_byte = vm->memory + vm->registers[IP];
		code = *vm->current_byte;
		if (code >= INST_COUNT) {
			printf("### Invalid instruction " NUMF " at " NUMF "\n", code, vm->registers[IP]);
			return 1;
		}

		if (has_flag(vm, VERBOSE)) {
			printf("### Processing instruction " NUMF " at " NUMF "\n", code, vm->registers[IP]);
		}

		inst = instructions[code];
		switch ((status = inst.func(vm))) {
		case 0:
			vm->registers[IP] = (vm->registers[IP] + inst.skip) % vm->memory_size;
			break;
		/* Halted */
		case -1:
			return *get_byte(vm, vm->registers[SP]);
		/* Jumped */
		case -2:
			break;
		default:
			perror("### VM crashed");
			return status;
		}
	}
	return 0;
}

void free_vm(vm_t *vm) {
	free(vm->memory);
	free(vm);
}
