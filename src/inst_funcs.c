#include "inst_funcs.h"
#include "options.h"
#include "vm.h"

#include <math.h>
#include <stdio.h>

/* Utility functions for instructions */

byte *get_byte(vm_t *vm, int addr) {
	if (addr > 0x7F) {
		return vm->registers + (addr & 0x7F);
	}
	return vm->memory + addr;
}

byte *next_byte(vm_t *vm, int offset) {
	return vm->memory + ((vm->registers[IP] + offset) % vm->memory_size);
}

int set_byte(vm_t *vm, int addr, byte set) {
	*get_byte(vm, addr) = set;
	return addr;
}

byte *get_addr(vm_t *vm, int offset) {
	return get_byte(vm, *get_byte(vm, vm->registers[IP] + offset));
}

byte set_flag(vm_t *vm, byte or) {
	return vm->registers[FLAGS] |= or;
}
byte has_flag(vm_t *vm, byte and) {
	return vm->registers[FLAGS] & and;
}

/* Instructions */

int jump_addr(vm_t *vm, int cond) {
	if (cond) {
		int addr = *next_byte(vm, 1);
		if (addr & 0x80) {
			fprintf(stderr, "### Cannot jump to register " NUMF "\n", addr);
			return 1;
		}
		vm->registers[IP] = addr;
		printf("--- Jumped to " NUMF "\n", addr);
		return -2;
	}
	return 0;
}

const char *number_place(int num) {
	switch (num % 10) {
	case 1:
		return "st";
	case 2:
		return "nd";
	case 3:
		return "rd";
	}
	return "th";
}

/* Instructions */

int instf_halt(vm_t *vm) {
	printf("### VM halted at " NUMF " with exit code " NUMF "\n", vm->registers[IP], *get_byte(vm, vm->registers[SP]));
	return -1;
}
int instf_print(vm_t *vm) {
	int addr = *next_byte(vm, 1);
	return !printf("--- Address " NUMF " contains " NUMF "\n", addr, *get_byte(vm, addr));
}
int instf_write(vm_t *vm) {
	byte *ptr = get_addr(vm, 1), c;
	int bytes = 0;
	for (bytes = 0; (c = *(ptr++)); bytes++) {
		putchar(c);
	}
	return 0;
}

int instf_set(vm_t *vm) {
	int addr = *next_byte(vm, 1);
	byte set = *next_byte(vm, 2);
	return !printf("--- Address " NUMF " set to " NUMF "\n", set_byte(vm, addr, set), set);
}
int instf_copy(vm_t *vm) {
	int from = *next_byte(vm, 1), to = *next_byte(vm, 2);
	byte value = *get_byte(vm, from);
	return !printf("--- Copied " NUMF " from " NUMF " to " NUMF "\n", value, from, set_byte(vm, to, value));
}

int instf_jmp(vm_t *vm) {
	return jump_addr(vm, 1);
}
int instf_jmp_eq(vm_t *vm) {
	return jump_addr(vm, vm->registers[AX] == *next_byte(vm, 2));
}
int instf_jmp_lt(vm_t *vm) {
	return jump_addr(vm, vm->registers[AX] < *next_byte(vm, 2));
}
int instf_jmp_gt(vm_t *vm) {
	return jump_addr(vm, vm->registers[AX] > *next_byte(vm, 2));
}

int instf_lshift(vm_t *vm) {
	byte *addr = &vm->registers[AX];
	byte bits = *next_byte(vm, 1);
	*addr = *addr << bits;
	return !printf("--- Shifted accum left by " NUMF "\n", bits);
}
int instf_rshift(vm_t *vm) {
	byte *addr = &vm->registers[AX];
	byte bits = *next_byte(vm, 1);
	*addr = *addr >> bits;
	return !printf("--- Shifted accum right by " NUMF "\n", bits);
}
int instf_not(vm_t *vm) {
	byte *addr = &vm->registers[AX];
	*addr = ~*addr;
	return !printf("--- Inverted accum\n");
}
int instf_or(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] |= num;
	return !printf("--- ORed accum with " NUMF "\n", num);
}
int instf_and(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] &= num;
	return !printf("--- ANDed accum with " NUMF "\n", num);
}
int instf_xor(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] ^= num;
	return !printf("--- XORed accum with " NUMF "\n", num);
}

int instf_add(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] += num;
	return !printf("--- Added " NUMF " to accum.\n", num);
}
int instf_sub(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] -= num;
	return !printf("--- Took " NUMF " from accum.\n", num);
}
int instf_mul(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] *= num;
	return !printf("--- Multiplied accum by " NUMF "\n", num);
}
int instf_div(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] /= num;
	return !printf("--- Divided accum by " NUMF "\n", num);
}
int instf_pow(vm_t *vm) {
	byte *addr = &vm->registers[AX];
	byte num = *next_byte(vm, 1);
	*addr = pow(*addr, num);
	return !printf("--- Raised accum to the %d%s power.\n", num, number_place(num));
}
int instf_mod(vm_t *vm) {
	byte num = *next_byte(vm, 1);
	vm->registers[AX] %= num;
	return !printf("--- Modulo'ed accum with " NUMF "\n", num);
}

int instf_push(vm_t *vm) {
	byte push = *next_byte(vm, 1);
	byte addr = --vm->registers[SP];
	return !printf("--- Pushed " NUMF " onto the stack at " NUMF "\n", push, set_byte(vm, addr, push));
}
int instf_pop(vm_t *vm) {
	int old = *get_byte(vm, vm->registers[SP]);
	return !printf("--- Popped " NUMF " from the stack at " NUMF "\n", old, vm->registers[SP]++);
}
