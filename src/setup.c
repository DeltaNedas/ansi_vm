#include "vm.h"
#include "instructions.h"
#include "inst_funcs.h"

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int set_rom_path(vm_t *vm, int argc, char **argv, int i, int extra) {
	extern int errno;
	if (extra + i >= argc) {
		fprintf(stderr, "* Expected ROM file for argument %d\n", extra + i);
		return errno = EINVAL;
	}

	vm->rom_path = argv[i + extra];
	return 0;
}

int parse_opt(vm_t *vm, char *opt, int *skip,
		int i, int argc, char **argv) {
	extern int errno;
	char c;
	int extra = 0;
	while ((c = *(opt++))) {
		switch (c) {
		case 'm':
			if (++extra + i >= argc) {
				fprintf(stderr, "* Expected memory size for argument %d\n", extra + i);
				return errno = EINVAL;
			}

			errno = 0;
			vm->memory_size = atoi(argv[i + extra]);
			if (errno) {
				fprintf(stderr, "* '%s' is not a number.\n", argv[i + extra]);
				return errno;
			}
			break;
		case 'v':
			set_flag(vm, VERBOSE);
			break;
		case 'r':
			if (set_rom_path(vm, argc, argv, i, ++extra)) {
				return errno;
			}
			break;
		default:
			fprintf(stderr, "* Unknown option '-%c' in arg #%d\n", c, i);
			return EINVAL;
		}
	}
	return 0;
}

/* return 0 on success */
int parse_opts(vm_t *vm, int argc, char **argv) {
	extern int errno;
	int skip = 0, i;
	char *arg;

	/* parse options */
	for (i = 1; i < argc; i++) {
		if (skip) {
			skip--;
			continue;
		}
		arg = argv[i];
		if (arg[0] == '-') {
			if ((errno = parse_opt(vm, arg + 1, &skip, i, argc, argv))) {
				return errno;
			}
		}
	}
	return 0;
}

vm_t *create_vm(int argc, char **argv) {
	/* 1. allocate vm struct */
	vm_t *vm = malloc(sizeof(vm_t));
	if (!vm) {
		perror("* Failed to allocate memory for vm");
		return NULL;
	}
	vm->memory_size = DEFAULT_MEMORY;
	vm->rom_path = NULL;
	memset(vm->registers, 0, REGISTER_COUNT);

	/* 2. parse options */
	if (argc > 1 && parse_opts(vm, argc, argv)) {
		goto cleanup;
	}

	/* 3. allocate vm memory */
	vm->memory = malloc(sizeof(byte) * vm->memory_size);
	if (!vm->memory) {
		fprintf(stderr, "* Failed to allocate %d ", vm->memory_size);
		perror("bytes for vm memory");
		goto cleanup;
	}
	printf("### Allocated %d bytes of memory for VM.\n", vm->memory_size);
	vm->current_byte = vm->memory;

	/* 4. return vm struct */
	return vm;

cleanup:
	free(vm);
	return NULL;
}
